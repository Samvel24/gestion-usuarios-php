# Gestión de usuarios usando php
Aplicación web para la gestión de usuarios usando PHP, MariaDB (SQL) HTML, CSS, JavaScript y jQuery.

## Configuración
- Requerimentos
1. PHP versión 8.2.8
2. Apache 2.4.56
3. MariaDB 10.5.19
4. jQuery 3.6.3

## Descripción de este proyecto
Este proyecto es útil para la gestión básica de usuarios, los datos de los usuarios que se pueden guardar en la base de datos son:
- Nombre
- Correo
- Teléfono

### Operaciones que se ejecutan en la base de datos
Las operaciones que se ejecutan en la base de datos son las siguientes:
- Creación de usuarios
- Lectura de usuarios
- Edicicón o modificación de usuarios
- Eliminación de usuarios

### Secciones de la aplicación
Los dos recursos a los que se puede acceder en esta aplicación son:
- localhost:{puerto}/
- localhost:{puerto}/actualizar.php?id={numeroDeId}

### Capturas de pantalla de la aplicación
* Pantalla de inicio de la aplicación web
![Pantalla de inicio de la aplicación web](/img/figura1.png)

* Pantalla de edición de datos un usuario
![Pantalla de edición de datos un usuario](/img/figura2.png)

