<?php

require_once 'clases/usuario.class.php';

$usuario = new Usuario();

// CREATE 
if (isset($_POST['Accion']) && $_POST['Accion'] == 'insertar') {
    if(isset($_POST['Nom']) && isset($_POST['Cor']) && isset($_POST['Tel'])) {
        $nombre = $_POST['Nom'];
        $correo = $_POST['Cor'];
        $telefono = $_POST['Tel'];
        $respuesta = $usuario->crearUsuario($nombre, $correo, $telefono);

        if($respuesta > 0) {
            // Devolvemos una fila HTML con los datos insertados en la DB
            $usuarioId = $respuesta;
            echo '
                <tr>
                    <td id="idUsuario"> ' . $usuarioId .' </td>
                    <td id="nombreUsuario"> ' . $nombre .' </td>
                    <td id="correoUsuario"> ' . $correo .' </td>
                    <td id="telefonoUsuario"> ' . $telefono .' </td>
                    <td><a href="actualizar.php?id='. $usuarioId .'">Editar</a></td>
                    <td><a id="borrar" href="#" onclick="borrarUsuario(this)">Borrar</a></td>
                </tr>
            ';
        }
        else {
            echo $respuesta;
        }
    }
}
// UPDATE
elseif (isset($_POST['Accion']) && $_POST['Accion'] == 'actualizar') {
    if(isset($_POST['ID']) && isset($_POST['Nom']) && isset($_POST['Cor']) && 
        isset($_POST['Tel'])) {
        $id = $_POST['ID'];
        $nombre = $_POST['Nom'];
        $correo = $_POST['Cor'];
        $telefono = $_POST['Tel'];
        $respuesta = $usuario->actualizarUsuario($id, $nombre, $correo, $telefono);

        echo $respuesta;
    }
}
// DELETE
elseif (isset($_POST['Accion']) && $_POST['Accion'] == 'borrar') {
    $usuarioId = $_POST['id'];
    $respuesta = $usuario->borrarUsuario($usuarioId);

    echo $respuesta;
}
?>