<?php
require_once "conexion/conexion.php";

$conexion = new Conexion();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Actulizar usuario</title>

    <link rel="stylesheet" href="estilo.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
</head>
<body>

    <?php 
        $id = $_GET['id']; 

        $consulta = "SELECT * FROM usuarios WHERE id = '$id'";
        $resultado = $conexion->obtenerDatos($consulta);
        $arregloFilaDB = array();

        foreach ($resultado as $key1 => $value1) {
            foreach ($value1 as $key2 => $value2) {
                $arregloFilaDB[$key2] = $value2;
            }
        }

        /* 
        Obtenemos los datos de la fila consultada y los colocamos en variables para poder
        usarlas comos valores predeterminados en cada input del formulario que nos ayudará
        a editar el usuario actual
        */
        $nombre = $arregloFilaDB['nombre'];
        $correo = $arregloFilaDB['correo'];
        $telefono = $arregloFilaDB['telefono'];
    ?>
    <h4>Actualización de datos del usuario con ID = <?php echo $id; ?></h4>
    <form method="POST" action="usuario.php" id="formulario">
        <div>
            <label for="nombre" class="form-label" id="labelNombre">Nombre</label>
            <input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $nombre; ?>" required maxlength="100">
        </div>
        <div>
            <label for="correo" class="form-label" id="labelCorreo">Correo</label>
            <input type="email" class="form-control" name="correo" id="correo" value="<?php echo $correo; ?>" required maxlength="100">
        </div>
        <div>
            <label for="telefono" class="form-label" id="labelTelefono">Telefono</label>
            <!--Permitimos un máximo de 12 números en el input para escribir el telefono-->
            <input type="number" class="form-control" name="telefono" id="telefono" value="<?php echo $telefono; ?>" required min="1" max="999999999999">
        </div>
        <div>
            <button type="button" name="actualizar" id="actualizar">Actualizar usuario</button>
        </div>
    </form>

    <script>
        $('#actualizar').click(function() {
            var idUsuario = "<?php echo $id ?>"
            var Nombre = document.getElementById("nombre").value;
            var Correo = document.getElementById("correo").value;
            var aux = document.getElementById("telefono").value;
            var Telefono = aux.toString();
            var atributoBoton = $('#actualizar').attr('name');

            console.log(Nombre);
            console.log(Correo);
            console.log(Telefono);
            console.log(atributoBoton);

            if(idUsuario != "" && Nombre != "" && Correo != "" && Telefono != "") {
                var datosFila = "ID=" + idUsuario + "&Nom=" + Nombre + "&Cor=" + Correo 
                    + "&Tel=" + Telefono + "&Accion=" + atributoBoton;
            }
            else {
                alert("Faltan datos por agregar al formulario");
            }

            $.ajax({
                data: datosFila,
                url: "usuario.php",
                type: "POST",
            })
            .done(
                function(respuestaServidor) {
                    alert(respuestaServidor);

                    /* 
                    Redireccionamos a la página de inicio del crud una vez que se ha 
                    actualizado correctamente al usuario
                    Ayuda obtenida de: https://stackoverflow.com/a/76817405
                    */
                    location.replace("/");
                }
            )
        }); 
    </script>
</body>
</html>
