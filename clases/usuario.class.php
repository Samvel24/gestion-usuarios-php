<?php

require_once "conexion/conexion.php";

class Usuario
{
    private $tabla;

    public function __construct() {
        $this->tabla = "usuarios";
    }

    // CREATE
    public function crearUsuario($nombre, $correo, $telefono) {
        $conexion = new Conexion();
        $consulta = "INSERT INTO $this->tabla (nombre, correo, telefono) VALUES (
            '$nombre', '$correo',  '$telefono')";

        $id = $conexion->idUltimaConsulta($consulta);
        
        if ($id > 0) {
            return $id;
        }
        else {
            return "Error interno del servidor, usuario no creado";
        }
    }

    // UPDATE
    public function actualizarUsuario($id, $nombre, $correo, $telefono) {
        $conexion = new Conexion();
        $consulta = "UPDATE $this->tabla SET nombre = '$nombre', correo = '$correo', 
            telefono = '$telefono' WHERE id = '$id'";

        $filas = $conexion->filasAfectadas($consulta);
        
        if($filas >= 1) {
            return "El usuario con ID = $id se ha actualizado correctamente";
        }
        else {
            return "Error interno del servidor, usuario no actualizado";
        }
    }

    // DELETE
    public function borrarUsuario($id) {
        $conexion = new Conexion();
        $consulta = "DELETE FROM $this->tabla WHERE id = '$id'";

        $filas = $conexion->filasAfectadas($consulta);
        
        if($filas >= 1) {
            return "El usuario con ID = $id se ha borrado correctamente";
        }
        else {
            return "Error interno del servidor, el usuario no se ha eliminado";
        }
    }
}

?>