<?php
require_once "conexion/conexion.php";

$conexion = new Conexion();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gestión usuarios</title>

    <link rel="stylesheet" href="estilo.css">
    <!-- Host de jQuery: https://developers.google.com/speed/libraries#jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
</head>
<body>
    <h4>Aplicación web para la gestión de usuarios</h4>
    <form method="POST" action="usuario.php" id="formulario">
        <div>
            <label for="nombre" class="form-label" id="labelNombre">Nombre</label>
            <input type="text" class="form-control" name="nombre" id="nombre" required maxlength="100">
        </div>
        <div>
            <label for="correo" class="form-label" id="labelCorreo">Correo</label>
            <input type="email" class="form-control" name="correo" id="correo" required maxlength="100">
        </div>
        <div>
            <label for="telefono" class="form-label" id="labelTelefono">Telefono</label>
            <!--Permitimos un máximo de 12 números en el input para escribir el telefono-->
            <input type="number" class="form-control" name="telefono" id="telefono" required required min="1" max="999999999999">
        </div>
        <div>
            <button type="button" class="btn btn-primary" name="insertar" id="enviar">Crear usuario</button>
        </div>
    </form>

    <br /><br />
    <h4>Usuarios guardados</h4>
    <div>
        <table class="table" id="tablaUsuarios">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Usuario</th>
                    <th>Correo</th>
                    <th>Telefono</th>
                    <th>Editar</th>
                    <th>Borrar</th>
                </tr>
            </thead>
        <?php
            // READ
            $filas = $conexion->obtenerFilasTabla();
            
            foreach ($filas as $key => $value) { // abrimos llaves para el ciclo foreach
                $id = $value['id'];
                $nombre = $value['nombre'];
                $correo = $value['correo'];
                $telefono = $value['telefono'];
        ?>
                <tr> <!--En cada iteración colocamos una fila HTML con los datos de la DB-->
                    <td id="idUsuario"><?php echo $id; ?></td>
                    <td id="nombreUsuario"><?php echo $nombre; ?></td>
                    <td id="correoUsuario"><?php echo $correo; ?></td>
                    <td id="telefonoUsuario"><?php echo $telefono; ?></td>
                    <td><a id="editar" href="actualizar.php?id=<?php echo $id ?>">Editar</a></td>
                    <td><a id="borrar" href="#borrar" onclick="borrarUsuario(this)">Borrar</a></td>
                </tr>
        <?php
            } // cerramos el ciclo foreach usando un bloque de código PHP
        ?>
            <tbody id="nuevasFilas">

            </tbody>
        </table>
    </div>

    <script>
        $('#enviar').click(function() { // al dar clic en el boton cuyo id es enviar
            var Nombre = document.getElementById("nombre").value;
            var Correo = document.getElementById("correo").value;
            var aux = document.getElementById("telefono").value;
            var Telefono = aux.toString();
            var atributoBoton = $('#enviar').attr('name');
            
            console.log(Nombre);
            console.log(Correo);
            console.log(Telefono);
            console.log(atributoBoton);
            
            if(Nombre != "" && Correo != "" && Telefono != "") {
                var datosForm = "Nom=" + Nombre + "&Cor=" + Correo + "&Tel=" + Telefono
                                + "&Accion=" + atributoBoton;
            }
            else {
                alert("Faltan datos por agregar al formulario");
            }

            $.ajax({
                data: datosForm,
                url: "usuario.php",
                type: "POST",
            })
            .done(
                function(respuestaServidor) {
                    // Si la respuesta del servidor NO incluye la subcadena 'Error'
                    if(!respuestaServidor.includes("Error")) {
                        alert("Nuevo usuario creado correctamente");
                        // Agregamos nuevas filas debajo de las que ya existen en la tabla
                        $('#nuevasFilas').append(respuestaServidor);
                    }
                    else {
                        alert(respuestaServidor);
                    }
                }
            )
            .fail(
                function() {
                    console.log("error en llamada ajax");
                }
            )
        });

        function borrarUsuario(elemento) {
            // Obtenemos el id de la fila del enlace 'Borrar' en la que se ha hecho clic
            /*
            Se ha hecho clic en un elemento <a>, este tiene como padre a un <td>, 
            este <td> tiene como padre a un <tr>, este <tr> tiene varios hijos, elegimos
            el primero de ellos y de este obtenemos el valor (html()) que hay en su interior
            */
            var idUsuario = $(elemento).parent().parent().children().first().html();
            var atributoEnlace = $(elemento).attr('id');
            
            console.log(idUsuario);
            console.log(atributoEnlace);

            var datosFila = "id=" + idUsuario + "&Accion=" + atributoEnlace;
            
            $.ajax({
                data: datosFila,
                url: "usuario.php",
                type: "POST",
            })
            .done(
                function(respuestaServidor) {
                    alert(respuestaServidor);

                    /* 
                    Accedemos al elemento <tr> (toda la fila del elemento en que se hizo clic)
                    y lo eliminamos con remove()
                    */
                    $(elemento).parent().parent().remove();
                }
            )
            .fail(
                function() {
                    console.log("error en llamada ajax");
                }
            )
        }
    </script>
</body>
</html>