<?php

class Conexion 
{
    private $servidor;
    private $usuario;
    private $contrasenia;
    private $baseDatos;
    private $conexion;

    public function __construct() {
        $this->servidor = "localhost";
        $this->usuario = "samuel2406";
        $this->contrasenia = "";
        $this->baseDatos = "crud_php";

        $this->conexion = new mysqli($this->servidor, $this->usuario, $this->contrasenia, 
                                    $this->baseDatos);

        if($this->conexion->connect_errno) {
            echo "La conexión con la base de datos ha sido incorrecta";
            die();
        }
    }

    public function obtenerFilasTabla() {
        $consulta = "SELECT * FROM usuarios";
        $resultado = $this->conexion->query($consulta);

        $filas = $resultado->fetch_all(MYSQLI_ASSOC);

        return $filas;
    }

    public function obtenerDatos($consulta) {
        $resultado = $this->conexion->query($consulta);
        $resultadoArray = array();

        foreach($resultado as $llave) {
            $resultadoArray[] = $llave;
        }

        return $this->convertirUTF8($resultadoArray);
    }

    private function convertirUTF8($array) {
        /*
        Con array_walk_recursive() se aplica una función de usuario (de manera recursiva)
        a cada miembro de un array, en este caso se realiza de manera recursiva la detección
        de codificación de caracteres (verifica si es utf-8 de manera estricta 
        (tercer argumento con valor de true)) en el arreglo $array
        Nota: La variable $item se pasa por referencia (&) para que el callback function() 
        pueda modificarla internamente
        */
        array_walk_recursive($array, function(&$item, $key) {
            if(!mb_detect_encoding($item, 'utf-8', true)) {
                /* en caso de que $item no sea utf-8, lo convertimos a esa codificación 
                con utf8_encode()
                */
                $item = utf8_encode($item);
            }
        });

        return $array;
    }

    public function idUltimaConsulta($consulta) {
        $result = $this->conexion->query($consulta);
        
        return $this->conexion->insert_id;
    }

    public function filasAfectadas($consulta) {
        $result = $this->conexion->query($consulta);

        return $this->conexion->affected_rows;
    }
}

?>
